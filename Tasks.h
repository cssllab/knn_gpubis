
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <omp.h>
#include <cassert>
#include <fstream>
#include <memory.h>
#include <cstdio>
#include <stdlib.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>
#include "mpi.h"
#include <inttypes.h>
#include <vector>
#include <algorithm>
using namespace std;


int    height;
int    width;
int    Th;
int sp;

int DATA_SIZE;
int RESULTS_SIZE;
extern float* Key1;
extern int* Row1;
extern int* Col1;
extern float* Key2;
extern int* Row2;
extern int* Col2;


extern float* dataArray1;
extern float* dataArray2;
extern float* ResultsKeys;

extern float* QDiag;
extern float* Vec;
extern float* H;

extern int* ResultsRows;
extern int* ResultsCols;

int numprocs;
int myid;
int numfiles;
int RowF;
int ColF;
long S;
MPI_Status   status;
MPI_Request  send_request,recv_request;


void SortResults(int);
void allocatePinnedMem(),knnEuclidean(),knnPearson(),freeMem(),freeRes();


