#include <thrust/sort.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <omp.h>
#include <cassert>
#include <fstream>
#include <memory.h>
#include <cstdio>
#include <stdlib.h>
#include <math.h>
#include <cublas.h>
#include <cuda.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>
#include <inttypes.h>

using namespace std;
#define inttype int

extern int    height;
extern int    width;
extern int    Th;
extern int sp;

extern int DATA_SIZE;
extern int RESULTS_SIZE;
float *dataArray1 ,*dataArray2,*resultsArray,*ResultsKeys,*Key1,*Key2,*QDiag,*Vec,*H = NULL;
int *Row1,*Row2,*Col1,*Col2,*ResultsCols = NULL;
int* ResultsRows;
float *tempkey;
int * tempValR,*tempValC;   

extern int numfiles;
extern int numprocs;

extern int RowF;
extern int ColF;

extern int myid;
int CheckDevices();
