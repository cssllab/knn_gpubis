#include "Tasks.h"
void internalmove(int row,int col)
{
	if (row ==0)
	{
		
		int shift = ((int)(col/numprocs))*2*Th*height;		
		memcpy(ResultsKeys + shift, Key2, height*Th*sizeof(float));
		memcpy(ResultsRows+shift, Row2, height*Th*sizeof(int));
		memcpy(ResultsCols + shift, Col2, height*Th*sizeof(int));

		if (col%numprocs == 0 && col !=0)
		{
			shift = Th*height;		
			memcpy(ResultsKeys + shift, Key1, height*Th*sizeof(float));
			memcpy(ResultsRows + shift, Row1, height*Th*sizeof(int));
			memcpy(ResultsCols + shift, Col1, height*Th*sizeof(int));
			shift = 0;	
			SortResults(shift);
		}
	}


	else if (row == col)
	{
		
		int shift = ((int)(row/numprocs))*2*Th*height + Th*height;		
		memcpy(ResultsKeys + shift, Key1, height*Th*sizeof(float));
		memcpy(ResultsRows + shift, Row1, height*Th*sizeof(int));
		memcpy(ResultsCols + shift, Col1, height*Th*sizeof(int));
		shift = ((int)(row/numprocs))*2*Th*height;	
		SortResults(shift);
	}
	else if (row%numprocs == col%numprocs)
	{
		int shift = ((int)(row/numprocs))*2*Th*height + Th*height;		
		memcpy(ResultsKeys + shift, Key1, height*Th*sizeof(float));
		memcpy(ResultsRows + shift, Row1, height*Th*sizeof(int));
		memcpy(ResultsCols + shift, Col1, height*Th*sizeof(int));
		shift = ((int)(row/numprocs))*2*Th*height;	
		SortResults(shift);
		
		shift = ((int)(col/numprocs))*2*Th*height + Th*height;		
		memcpy(ResultsKeys + shift, Key2, height*Th*sizeof(float));
		memcpy(ResultsRows + shift, Row2, height*Th*sizeof(int));
		memcpy(ResultsCols + shift, Col2, height*Th*sizeof(int));
		shift = ((int)(col/numprocs))*2*Th*height;	
		SortResults(shift);
	}
	else
	{
		int shift = ((int)(col/numprocs))*2*Th*height + Th*height;		
		memcpy(ResultsKeys + shift, Key2, height*Th*sizeof(float));
		memcpy(ResultsRows + shift, Row2, height*Th*sizeof(int));
		memcpy(ResultsCols + shift, Col2, height*Th*sizeof(int));
		shift = ((int)(col/numprocs))*2*Th*height;	
		SortResults(shift);

	
	}

	
}	
void communication(int row, int col)
{

	int bufsize,size1,size2,size3;
	void* buffer;

	MPI_Pack_size( height*Th, MPI_FLOAT, MPI_COMM_WORLD, &size1);
	MPI_Pack_size( height*Th, MPI_INT, MPI_COMM_WORLD, &size2);
	MPI_Pack_size( height*Th, MPI_INT, MPI_COMM_WORLD, &size3);

	bufsize = 3 * MPI_BSEND_OVERHEAD + size1 + size2+size3;
        buffer = malloc( bufsize );
    	MPI_Buffer_attach( buffer, bufsize);	
	
	if (myid != row%numprocs)
	{
		//Key1 S
		int Err = MPI_Bsend((void*)Key1, height*Th, MPI_FLOAT, row%numprocs, 0, MPI_COMM_WORLD);
		if (Err != MPI_SUCCESS)
			cout << "Send Key2 " << myid << " Error!" << endl;


		//Row1 S
		Err = MPI_Bsend((void*)Row1, height*Th, MPI_INT, row%numprocs, 1, MPI_COMM_WORLD);
		if (Err != MPI_SUCCESS)
			cout << "Send Row2 " << myid << " Error!" << endl;
		//Col1 S
		Err = MPI_Bsend((void*)Col1, height*Th, MPI_INT, row%numprocs, 2, MPI_COMM_WORLD);
		if (Err != MPI_SUCCESS)
			cout << "Send Col2 " << myid << " Error!" << endl;
	}

	else if (col%numprocs == row%numprocs && col + numprocs >= numfiles)
	{
		if (myid < numprocs-1)		
		for (int i = (myid+1); i<numprocs; i++)
		{
			//Key1 R
			int shift = ((int)(row/numprocs))*2*height*Th + height*Th;				
			int Err = MPI_Recv((void*)(ResultsKeys+shift), height*Th, MPI_FLOAT,i, 0, MPI_COMM_WORLD,&status);
			if (Err != MPI_SUCCESS)
				cout << "Recv Key2 " << myid << " Error!" << endl;
	

			//Row1 R

			Err = MPI_Recv((void*)(ResultsRows+shift), height*Th, MPI_INT, i, 1, MPI_COMM_WORLD,&status);
			if (Err != MPI_SUCCESS)
				cout << "Recv Row2 " << myid << " Error!" << endl;

			//Col1 R
			Err = MPI_Recv((void*)(ResultsCols+shift), height*Th, MPI_INT, i, 2, MPI_COMM_WORLD,&status);
			if (Err != MPI_SUCCESS)
				cout << "Recv Col2 " << myid << " Error!" << endl;
			shift = ((int)(row/numprocs))*2*Th*height;
			SortResults(shift);	
		}
	}
	else 
	{
		int shift;
		for (int i = (myid+1)%numprocs; i!=myid; i = (i+1)%numprocs)
		{
			//Key1 R

	                shift = ((int)(row/numprocs))*2*height*Th + height*Th;								
			int Err = MPI_Recv((void*)(ResultsKeys+shift), height*Th, MPI_FLOAT,i, 0, MPI_COMM_WORLD,&status);
			if (Err != MPI_SUCCESS)
				cout << "Recv Key2 " << myid << " Error!" << endl;
	

			//Row1 R

			Err = MPI_Recv((void*)(ResultsRows+shift), height*Th, MPI_INT, i, 1, MPI_COMM_WORLD,&status);
			if (Err != MPI_SUCCESS)
				cout << "Recv Row2 " << myid << " Error!" << endl;
			//Col1 R
			Err = MPI_Recv((void*)(ResultsCols+shift), height*Th, MPI_INT, i, 2, MPI_COMM_WORLD,&status);
			if (Err != MPI_SUCCESS)
				cout << "Recv Col2 " << myid << " Error!" << endl;
			shift = ((int)(row/numprocs))*2*Th*height;	
			SortResults(shift);
		}
		
	}

	
	
	
	MPI_Buffer_detach(&buffer, &bufsize);	
	free(buffer);
	
	
}
void ReadFilesB(char *file)
{

	long shift1 = 0;
	int count;
	MPI_File fh;
	MPI_Status status;
	float* TEMPd = (float*)malloc((DATA_SIZE/numprocs)*sizeof(float));
	int file_open_error = MPI_File_open(MPI_COMM_SELF, file, 
				  MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);

	if (file_open_error != MPI_SUCCESS) 
	{

	    char error_string[BUFSIZ];
	    int length_of_error_string, error_class;

	    MPI_Error_class(file_open_error, &error_class);
	    MPI_Error_string(error_class, error_string, &length_of_error_string);
	    printf("%3d: %s\n", myid, error_string);
	    
	    MPI_Error_string(file_open_error, error_string, &length_of_error_string);
	    printf("%3d: %s\n", myid, error_string);
	    
	    MPI_Abort(MPI_COMM_WORLD, file_open_error);
	}
	

	MPI_File_seek(fh, myid*(DATA_SIZE/numprocs)*sizeof(float), MPI_SEEK_SET); 

	MPI_File_read(fh, TEMPd, DATA_SIZE/numprocs, MPI_FLOAT, &status);		
       
        MPI_Get_count(&status, MPI_BYTE, &count);

	if (count != (DATA_SIZE/numprocs)*sizeof(float)) 
		printf("Error in count reading in node %d\n", myid);
	
        MPI_File_close(&fh);

	int bufsize,size1,bbufsize;
	void* buffer;


	MPI_Pack_size( DATA_SIZE/numprocs, MPI_FLOAT, MPI_COMM_WORLD, &size1);


	bufsize = MPI_BSEND_OVERHEAD + size1;
        buffer = malloc(bufsize);
    	MPI_Buffer_attach(buffer, bufsize);	
	
	if (myid != 0)
	{
		int Err = MPI_Bsend((void*)TEMPd, DATA_SIZE/numprocs, MPI_FLOAT, 0, 0, MPI_COMM_WORLD);
		if (Err != MPI_SUCCESS)
			cout << "Send TEMPd " << myid << " Error!" << endl;

	}


	if (myid == 0) 
	{
		int shiftT;		
		for (int i = (myid+1); i<numprocs; i++)
		{
			shiftT = i*DATA_SIZE/numprocs;				
			int Err = MPI_Recv((void*)(dataArray1+shift1+shiftT), DATA_SIZE/numprocs, MPI_FLOAT,i, 0, MPI_COMM_WORLD,&status);
			if (Err != MPI_SUCCESS)
				cout << "Recv TEMPd " << myid << " Error!" << endl;
			memcpy(dataArray1+shift1,TEMPd,(DATA_SIZE/numprocs)*sizeof(float));
		}

	}

	

	MPI_Buffer_detach(&buffer, &bufsize);

	free(buffer);
	free(TEMPd);

	TEMPd = (float*)malloc(DATA_SIZE*sizeof(float));	

	if (myid ==0)	
		memcpy(TEMPd, dataArray1+shift1,DATA_SIZE*sizeof(float));	
	MPI_Pack_size(DATA_SIZE, MPI_FLOAT, MPI_COMM_WORLD, &size1);

	bufsize = MPI_BSEND_OVERHEAD + size1;
        buffer = malloc(bufsize);
    	MPI_Buffer_attach(buffer, bufsize);

	int Err = MPI_Bcast((void*)TEMPd, DATA_SIZE, MPI_FLOAT, 0, MPI_COMM_WORLD);

	if (Err != MPI_SUCCESS)
		cout << "Broadcast i " << myid << " Error!" << endl;
	if (myid !=0)	
		memcpy(dataArray1+shift1, TEMPd, DATA_SIZE*sizeof(float));	

	MPI_Buffer_detach(&buffer, &bufsize);		
	free(buffer);

	free(TEMPd);

	
}

void ReadFromLocalFiles(char *file)
{	

	double start = MPI_Wtime();

	struct stat st;
	
	stat(file, &st);

	long size = st.st_size;
	
	if (size != DATA_SIZE*sizeof(float))
	{
		cout<<"Error in File size"<<endl;
		printf ("file is %s and node %d\n",file,myid);		
	}
	ifstream data;
	data.open(file, ios::in | ios::binary);		
	if (data.is_open())		
	{
		data.seekg(0);
		
		data.read((char*)(dataArray2),DATA_SIZE*sizeof(float));

		if ((data.rdstate() & ifstream::failbit ) !=0) 
			cout<<"Error in File reading"<<endl;
		data.close();
	}
	else
	{
		cout<<"Error in File opening"<<endl;
		printf ("file is %s and node %d",file,myid);		
	}
	double finish = MPI_Wtime();
	//printf ("\nI/O time for %d is %f\n",myid, finish-start);

}

void Tasks()
{
	

			
	allocatePinnedMem();
	int s = numfiles*height*width;
	float * Data = (float*)malloc(numfiles*DATA_SIZE*sizeof(float));
	//if (myid ==0)	
	for (int i = 0;i<numfiles*height*width;i++)
		Data[i] = (float)rand()/RAND_MAX;

	/*char *f1 = "/SharedPartitionsDir";
	char *f2 = "/LocalPartitionsDir";
	char a[3];

	char fileShared[strlen(f1) +3];
	char fileLocal[strlen(f2) +3];*/


	double GPUtime, Commtime, internaltime = 0;
	double start1,finish1; 

	double start = MPI_Wtime();
	
	for (int i = 0; i<numfiles;i++)
	{

		memcpy(dataArray1,Data+i*DATA_SIZE,DATA_SIZE*sizeof(float));
		
		/*sprintf(a, "%d", i+1);		

		strcpy(fileShared, f1);
						
		strcat(file1,a);

		ReadFilesB(fileShared);*/


		for(int j = myid;j< numfiles;j= j+numprocs)
		{
			if (j  >= i && j<numfiles)
			{				
	
			
				memcpy(dataArray2,Data+j*DATA_SIZE,DATA_SIZE*sizeof(float));
				RowF =i;
				ColF = j;

				/*sprintf(a, "%d", j+1);		

				strcpy(fileShared, f2);
						
				strcat(file1,a);

				ReadFilesB(fileLocal);


				ReadFromLocalFiles(fileLocal);*/
					


				start1 = MPI_Wtime();			
				//knnPearson();
				knnEuclidean();
				finish1 = MPI_Wtime();


				GPUtime +=finish1-start1;
				
			
				
				start1 = MPI_Wtime();			
				internalmove(i,j); 
				finish1 = MPI_Wtime();    

				internaltime +=finish1-start1;


				start1 = MPI_Wtime();			
				communication(i,j);
				finish1 = MPI_Wtime();    

				Commtime +=finish1-start1;

				
			}

			
		}
		MPI_Barrier(MPI_COMM_WORLD);
		printf("Task Done Files row %d and node %d\n",i+1,myid);

	}


	freeMem();


	
	freeRes();

	double finish = MPI_Wtime();
	cout<<myid<<"GPUTaskTotal: "<<GPUtime<<endl;
	cout<<myid<<"InternalMove Total: "<<internaltime<<endl;
	cout<<myid<<"Communication Total: "<<Commtime<<endl;

	printf("\nnode %d TotalTasks %f\n",myid, finish - start);

}

	
int main(int argc, char *argv[])
{
	
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&numprocs); /* find out how big the SPMD world is */
	MPI_Comm_rank(MPI_COMM_WORLD,&myid); /* and this processes' rank is */


	numfiles     = atoi(argv[1]);   // partitions (P)
	height   = atoi(argv[2]);   // each partition point number,     max=65535
	width        = atoi(argv[3]);     // data dimension,    max=8192
	Th          = atoi(argv[4]);     // Nearest neighbors to consider
	sp = atoi(argv[5]);           //GPU partitions (R)
	DATA_SIZE = height*width; 
	RESULTS_SIZE = height*height;
	Tasks();

	

	MPI_Finalize();

	return 0;
}



