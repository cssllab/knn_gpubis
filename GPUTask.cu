#include "GPUTask.h"
__global__ void CoalescedValMove(float*Kptr,int*VptrRow,int*VptrCol,float*KptrRow, int*VptrRowR,int*VptrRowC,int H,int thresh,int colsize)
{	

	int threadx = blockDim.x * blockIdx.x + threadIdx.x;
	int i = threadIdx.y; 
	if (threadx < H)	
	
	{
		KptrRow[threadx*thresh+i] = Kptr[threadx*colsize + i];
		VptrRowR[threadx*thresh+i] = VptrRow[threadx*colsize + i];
		VptrRowC[threadx*thresh+i] = VptrCol[threadx*colsize + i];
	}
	//if (i == 0&& threadx == 0)	
		//printf("i %d threadx %d \n",i,threadx);

}
__global__ void CoalescedValMoveF(float*KptrRow, int*VptrRowR,int*VptrColR,float*FKptr,int*FVptrR,int*FVptrC,int H,int th,int thresh,int colsize)
{	
	int threadx = blockDim.x * blockIdx.x + threadIdx.x;
	int i = threadIdx.y; 
	if (threadx < H)	
	
	{
		FKptr[threadx*thresh+i] = KptrRow[threadx*colsize + i];
		FVptrR[threadx*thresh+i] = VptrRowR[threadx*colsize + i];
		FVptrC[threadx*thresh+i] = VptrColR[threadx*colsize + i];
	}
	//if (threadx == 0&& i == 0)	
		//printf("y %d thread %d th %d colsize %d\n",i,threadx,th,colsize);
}
__global__ void IndInitKernel(int *RowMat, int* ColMat,int RowStart,int ColStart, int H)
{
	int row = blockDim.x * blockIdx.x + threadIdx.x;
	int col = blockIdx.y;
		
	if (row < H)	
	{
		RowMat[col*H+ row] = RowStart + row;
		ColMat[col*H + row] = ColStart + col;
		
	}
	//if (col == 0&& row == 0)	
		//printf("row %d col %d rowS %d colS %d \n",RowMat[0],ColMat[0],RowStart,ColStart);
}

__global__ void MeanNormKernel(float*A,float*Norm,float*Mean,int cols,int rows)
{
	int ColIdx = blockIdx.x;
	__shared__ float S_NormTemp;
	__shared__ float S_MeanTemp;
	float Vartemp,NormTemp,MeanTemp = 0.0f;
 
	int RowIdx = threadIdx.x;
	if (RowIdx == 0)
	{S_NormTemp = 0; S_MeanTemp = 0;}
	__syncthreads();	
	for (RowIdx = threadIdx.x;RowIdx<rows;RowIdx+=blockDim.x)		
		if (RowIdx < rows)
		{	
			Vartemp = A[ColIdx*rows+RowIdx];			
			NormTemp += Vartemp*Vartemp;
			MeanTemp += Vartemp;
		}
	
	atomicAdd(&S_NormTemp,NormTemp);
	atomicAdd(&S_MeanTemp,MeanTemp);
	__syncthreads();	
	if (threadIdx.x == 0)
	{
		Norm[ColIdx] = S_NormTemp;	
		Mean[ColIdx] = S_MeanTemp; 
	}
	__syncthreads();	

	//if ((threadIdx.x == 0) && (ColIdx ==0))
		//printf("Norm:%f Mean:%f \n",Norm[ColIdx],Mean[ColIdx]);

}
__global__ void ZScore(float*A,float*Norm,float*Mean,int cols,int rows)
{
	__shared__ float S_NormTemp;
	__shared__ float S_MeanTemp;	
	int ColIdx = blockIdx.x;
	float Vartemp,Datatemp = 0.0f; 
	
	int RowIdx = threadIdx.x;
	if (RowIdx == 0)
	{S_NormTemp = Norm[ColIdx]; S_MeanTemp = Mean[ColIdx];}
	__syncthreads();

	Vartemp = sqrt(S_NormTemp/rows - (S_MeanTemp/rows)*(S_MeanTemp/rows));	

	float MeanTemp = S_MeanTemp;

	for (RowIdx = threadIdx.x;RowIdx<rows;RowIdx+=blockDim.x)		
		if (RowIdx < rows)
		{	
			Datatemp = (A[ColIdx*rows+RowIdx] - MeanTemp)/Vartemp;			

			A[ColIdx*rows+RowIdx] = Datatemp/rows;
		}

}
__global__ void Summation(float*Res,float*NA,float*NB,int cols,int rows)
{
	int ColIdx = blockIdx.x;
	__shared__ float S_NormB;
	
	int RowIdx = threadIdx.x;
	if (RowIdx == 0)
		S_NormB = NB[ColIdx];
	__syncthreads();
	float Datatemp;	
	for (RowIdx = threadIdx.x;RowIdx<rows;RowIdx+=blockDim.x)	
		if (RowIdx < rows)	
		{
			Datatemp = Res[ColIdx*rows+RowIdx];
			//if ((RowIdx ==5)&&(ColIdx == 5))
				//printf("NormA %f , NormB %f , Res %f\n",NA[0],NB[0],Res[0]);
			Res[ColIdx*rows+RowIdx] = Datatemp+S_NormB + NA[RowIdx];
		}



}

__global__ void CoalescedValSort(int *RowMat, int* ColMat,int RowStart,int ColStart, int H)
{
	int row = blockDim.x * blockIdx.x + threadIdx.x;
	int col = blockIdx.y;
		
	if (row < H)	
	{
		RowMat[col*H+ row] = RowStart + row;
		ColMat[col*H + row] = ColStart + col;
		
	}
	//if (col == 0&& row == 0)	
		//printf("row %d col %d rowS %d colS %d \n",RowMat[0],ColMat[0],RowStart,ColStart);
}
__global__ void NormKernel(float*A,float*Norm,float*Mean,int cols,int rows)
{
	int ColIdx = blockIdx.x;
	__shared__ float S_NormTemp;
	float Vartemp,NormTemp = 0;
 
	int RowIdx = threadIdx.x;
	if (RowIdx == 0)
		S_NormTemp = 0;
	__syncthreads();	
	for (RowIdx = threadIdx.x;RowIdx<rows;RowIdx+=blockDim.x)		
		if (RowIdx < rows)
		{	
			Vartemp = A[ColIdx*rows+RowIdx];			
			NormTemp += Vartemp*Vartemp;

		}

	atomicAdd(&S_NormTemp,NormTemp);
	__syncthreads();	
	if (threadIdx.x == 0)
	{
		Norm[ColIdx] = S_NormTemp;	
	}
	__syncthreads();	
}
void knnEuclidean()
{


	float GPUtime = 0;float DistanceTime=0;float SortingTime = 0;float SortInitTime = 0;float SortMoveTime = 0;
	float InitTime = 0; float MemTransTime = 0;
	float MeanNormTime = 0; float SummationTime = 0;
	
	int Row = height; int Col = width; int k = Th;		
	cudaEvent_t start, stop;

	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);


	float *d_A,*d_B;

	cublasInit();	
	int Data_size = Row * Col;
	int Res_Size = Row*Row;
	cublasAlloc(Data_size/sp,sizeof(float),(void**)&d_A);

	if (cublasGetError()!= CUBLAS_STATUS_SUCCESS)
	{
		std::cerr<<"cublasAllocA has an Error!"<<std::endl;
		printf("\n%d\n",cublasGetError());		
		exit(EXIT_FAILURE);
	}

	cublasAlloc(Data_size,sizeof(float),(void**)&d_B);
	if (cublasGetError()!= CUBLAS_STATUS_SUCCESS)
	{
		std::cerr<<"cublasAllocB has an Error!"<<std::endl;
		printf("\n%d\n",cublasGetError());		
		exit(EXIT_FAILURE);
	}

	thrust::device_vector<float> d_Res(Res_Size/sp);
	thrust::device_vector<int> d_Row(Res_Size/sp);
	thrust::device_vector<int> d_Col(Res_Size/sp);
	
	float * Resptr = thrust::raw_pointer_cast(&d_Res[0]);
	int * Rowptr = thrust::raw_pointer_cast(&d_Row[0]);
	int * Colptr = thrust::raw_pointer_cast(&d_Col[0]);

	thrust::device_vector<float> d_ResAux(Res_Size/sp);
	thrust::device_vector<int> d_ColAux(Res_Size/sp);
	
	float * ResAuxptr = thrust::raw_pointer_cast(&d_ResAux[0]);
	int * ColAuxptr = thrust::raw_pointer_cast(&d_ColAux[0]);


	thrust::device_vector<float> d_NA(Row/sp);
	float * NAptr = thrust::raw_pointer_cast(&d_NA[0]);
	
	thrust::device_vector<float> d_uA(Row/sp);
	float * uAptr = thrust::raw_pointer_cast(&d_uA[0]);

	thrust::device_vector<float> d_NB(Row);
	float * NBptr = thrust::raw_pointer_cast(&d_NB[0]);

	thrust::device_vector<float> d_uB(Row);
	float * uBptr = thrust::raw_pointer_cast(&d_uB[0]);


	thrust::device_vector<float> d_Kvals(Row*k);
	float * Kvalsptr = thrust::raw_pointer_cast(&d_Kvals[0]);


	thrust::device_vector<int> d_KRows(Row*k);
	int * KRowsptr = thrust::raw_pointer_cast(&d_KRows[0]);


	thrust::device_vector<int> d_KCols(Row*k);
	int * KColsptr = thrust::raw_pointer_cast(&d_KCols[0]);

	
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&GPUtime, start, stop);
	InitTime += GPUtime; 

	
	cudaEventRecord(start, 0);

	cublasSetMatrix (Col, Row,sizeof(float),dataArray2, Col, d_B,Col);
	if (cublasGetError()!= CUBLAS_STATUS_SUCCESS)
	{
		std::cerr<<"cublasSetB has an Error!"<<std::endl;
		printf("\n%d\n",cublasGetError());		
		exit(EXIT_FAILURE);
	}

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&GPUtime, start, stop);
	MemTransTime += GPUtime; 

	
	cudaEventRecord(start, 0);

	dim3 threadsPerBlock,blocksPerGrid;
	threadsPerBlock.x = 64;
	threadsPerBlock.y = 1;
	blocksPerGrid.x = (unsigned int)Row;
	blocksPerGrid.y = (unsigned int)1;
	NormKernel<<<blocksPerGrid, threadsPerBlock>>>(d_B,NBptr,uBptr,Row,Col);

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&GPUtime, start, stop);

	MeanNormTime += GPUtime; 
	
	float* keycheck = (float*)malloc(Row*(Row/sp)*sizeof(float));
	
	int shift;

	for (int i = 0;i<sp;i++)
	{

	
		cudaEventRecord(start, 0);
	
		cublasSetMatrix (Col, Row/sp,sizeof(float),dataArray1+ i*Data_size/sp, Col, d_A,Col);
		if (cublasGetError()!= CUBLAS_STATUS_SUCCESS)
		{
			std::cerr<<"cublasSetA has an Error!"<<std::endl;
			printf("\n%d\n",cublasGetError());		
			exit(EXIT_FAILURE);
		}	

		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);
		MemTransTime += GPUtime; 		

		
	
		cudaEventRecord(start, 0);

		threadsPerBlock.x = 64;
		threadsPerBlock.y = 1;
		blocksPerGrid.x = (unsigned int)Row/sp;
		blocksPerGrid.y = (unsigned int)1;
		NormKernel<<<blocksPerGrid, threadsPerBlock>>>(d_A,NAptr,uAptr,Row/sp,Col);

		
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);
		MeanNormTime += GPUtime; 

	
		cudaEventRecord(start, 0);
		
		cudaEventRecord(start, 0);			

		cublasSgemm ('t', 'n', Row/sp,Row,Col, -2 , d_A, Col,d_B, Col, 0,Resptr, Row/sp);
		
		if (cublasGetError()!= CUBLAS_STATUS_SUCCESS)
		{
			std::cerr<<"cublasSgemm has an Error!"<<std::endl;
			printf("\n%d\n",cublasGetError());		
			exit(EXIT_FAILURE);
		}
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);
		
		DistanceTime += GPUtime;

	
		cudaEventRecord(start, 0);

		threadsPerBlock.x = 64;
		threadsPerBlock.y = 1;
		blocksPerGrid.x = (unsigned int)Row;
		blocksPerGrid.y = (unsigned int)1;
		Summation<<<blocksPerGrid, threadsPerBlock>>>(Resptr,NAptr,NBptr,Row,Row/sp);

		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);
		
		SummationTime += GPUtime; 	
	
		threadsPerBlock.x = 64;
		threadsPerBlock.y = 1;
		blocksPerGrid.x = (unsigned int)Row/(sp*threadsPerBlock.x);
		blocksPerGrid.y = (unsigned int)Row;
		CoalescedValSort<<<blocksPerGrid, threadsPerBlock>>>(Rowptr,Colptr, i*(Row/sp),0,Row/sp);



		cudaMemcpy(ResAuxptr,Resptr,(Res_Size/sp)*sizeof(float),cudaMemcpyDeviceToDevice);
		cudaMemcpy(ColAuxptr,Colptr,(Res_Size/sp)*sizeof(int),cudaMemcpyDeviceToDevice);

		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);
		SortInitTime += GPUtime;

		
		cudaEventRecord(start, 0);			



		thrust::sort_by_key(d_Res.begin(), d_Res.end(),make_zip_iterator(make_tuple(d_Row.begin(),d_Col.begin())));
		thrust::stable_sort_by_key(d_Row.begin(),d_Row.end(),make_zip_iterator(make_tuple(d_Res.begin(),d_Col.begin())));	

		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);
	
		SortingTime +=GPUtime;
		
		cudaEventRecord(start, 0);			

		shift = i*k*Row/sp;
		threadsPerBlock.x = 1;
		threadsPerBlock.y = k;
		blocksPerGrid.x = Row/(sp*threadsPerBlock.x);
		blocksPerGrid.y = 1;
		CoalescedValMove<<<blocksPerGrid, threadsPerBlock>>>(Resptr,Rowptr,Colptr,Kvalsptr+shift, KRowsptr+shift,KColsptr+shift,Row/sp,k,Row);	


		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);

		SortMoveTime+=GPUtime;


	}

	
	cudaEventRecord(start, 0);	
		
	float* kVals = (float*)malloc(Row*k*sizeof(float));
	int* kRows = (int*)malloc(Row*k*sizeof(int));
	int* kCols = (int*)malloc(Row*k*sizeof(int));

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&GPUtime, start, stop);
	InitTime +=GPUtime;

	
	cudaEventRecord(start, 0);			

 	thrust::copy(d_Kvals.begin(), d_Kvals.end(), kVals);
 	thrust::copy(d_KRows.begin(), d_KRows.end(), kRows);
 	thrust::copy(d_KCols.begin(), d_KCols.end(), kCols);
	
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&GPUtime, start, stop);
	MemTransTime += GPUtime; 

	
	cublasFree(d_A);
	cublasFree(d_B);	
	
	cublasShutdown();
	free(kVals);free(kRows);free(kCols);


	cout<<"Initation took:"<<InitTime<<"ms"<<endl;
	cout<<"CPU/GPU Transfer took:"<<MemTransTime<<"ms"<<endl;
	cout<<"DistanceCalculation took:"<<DistanceTime<<"ms"<<endl;
	cout<<"MeanNorm took:"<<MeanNormTime<<"ms"<<endl;
	cout<<"Summation took:"<<SummationTime<<"ms"<<endl;
	cout<<"Sorting took:"<<SortingTime<<"ms"<<endl;
	cout<<"Index Initiation took:"<<SortInitTime<<"ms"<<endl;
	cout<<"Extracting Sorted Values took:"<<SortMoveTime<<"ms"<<endl;

}

void knnPearson()
{
	
	cudaThreadSynchronize();
	assert(cudaGetLastError() ==cudaSuccess);	

	float GPUtime = 0;float GemmTime=0;float SortingTime = 0;float SortInitTime = 0;float SortMoveTime = 0;
	float InitTime = 0; float MemTransTime = 0;
	float MeanNormTime = 0; float SummationTime = 0;float GPUMemTrans = 0;
	
			
	cudaEvent_t start, stop;

	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);
	int Row = height; int Col = width; int k = Th;

	float *d_A,*d_B;

	cublasInit();	
	int Data_size = Row * Col;
	int Res_Size = Row*Row;
	cublasAlloc(Data_size/sp,sizeof(float),(void**)&d_A);

	if (cublasGetError()!= CUBLAS_STATUS_SUCCESS)
	{
		std::cerr<<"cublasAllocA has an Error!"<<std::endl;
		printf("\n%d\n",cublasGetError());		
		exit(EXIT_FAILURE);
	}

	cublasAlloc(Data_size,sizeof(float),(void**)&d_B);
	if (cublasGetError()!= CUBLAS_STATUS_SUCCESS)
	{
		std::cerr<<"cublasAllocB has an Error!"<<std::endl;
		printf("\n%d\n",cublasGetError());		
		exit(EXIT_FAILURE);
	}

	thrust::device_vector<float> d_Res(Res_Size/sp);
	thrust::device_vector<int> d_RowInd(Res_Size/sp);
	thrust::device_vector<int> d_ColInd(Res_Size/sp);
	
	float * p_Res = thrust::raw_pointer_cast(&d_Res[0]);
	int * p_RowInd = thrust::raw_pointer_cast(&d_RowInd[0]);
	int * p_ColInd = thrust::raw_pointer_cast(&d_ColInd[0]);


	thrust::device_vector<float> d_NA(Row/sp);
	float * p_NA = thrust::raw_pointer_cast(&d_NA[0]);

	thrust::device_vector<float> d_NB(Row);
	float * p_NB = thrust::raw_pointer_cast(&d_NB[0]);

	thrust::device_vector<float> d_uA(Row/sp);
	float * p_uA = thrust::raw_pointer_cast(&d_uA[0]);

	thrust::device_vector<float> d_uB(Row);
	float * p_uB = thrust::raw_pointer_cast(&d_uB[0]);

	thrust::device_vector<float> d_KNN1(Row*k);
	float * p_KNN1 = thrust::raw_pointer_cast(&d_KNN1[0]);


	thrust::device_vector<int> d_KNNRowInd1(Row*k);
	int * p_KNNRowInd1 = thrust::raw_pointer_cast(&d_KNNRowInd1[0]);


	thrust::device_vector<int> d_KNNColInd1(Row*k);
	int * p_KNNColInd1 = thrust::raw_pointer_cast(&d_KNNColInd1[0]);
///Trans
	thrust::device_vector<float> d_KNN2(3*Row*k);
	float * p_KNN2 = thrust::raw_pointer_cast(&d_KNN2[0]);


	thrust::device_vector<int> d_KNNRowInd2(3*Row*k);
	int * p_KNNRowInd2 = thrust::raw_pointer_cast(&d_KNNRowInd2[0]);


	thrust::device_vector<int> d_KNNColInd2(3*Row*k);
	int * p_KNNColInd2 = thrust::raw_pointer_cast(&d_KNNColInd2[0]);
///TransTemp	
	thrust::device_vector<float> d_KNNt(Row*k);
	float * p_KNNt = thrust::raw_pointer_cast(&d_KNNt[0]);


	thrust::device_vector<int> d_KNNRowIndt(Row*k);
	int * p_KNNRowIndt = thrust::raw_pointer_cast(&d_KNNRowIndt[0]);


	thrust::device_vector<int> d_KNNColIndt(Row*k);
	int * p_KNNColIndt = thrust::raw_pointer_cast(&d_KNNColIndt[0]);

	
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&GPUtime, start, stop);
	InitTime += GPUtime; 


	cudaEventRecord(start, 0);

	cublasSetMatrix (Col, Row,sizeof(float),dataArray2, Col, d_B,Col);
	if (cublasGetError()!= CUBLAS_STATUS_SUCCESS)
	{
		std::cerr<<"cublasSetB has an Error!"<<std::endl;
		printf("\n%d\n",cublasGetError());		
		exit(EXIT_FAILURE);
	}

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&GPUtime, start, stop);
	MemTransTime += GPUtime; 


	cudaEventRecord(start, 0);

	dim3 threadsPerBlock,blocksPerGrid;
	threadsPerBlock.x = 64;
	threadsPerBlock.y = 1;
	blocksPerGrid.x = (unsigned int)Row;
	blocksPerGrid.y = (unsigned int)1;
	MeanNormKernel<<<blocksPerGrid, threadsPerBlock>>>(d_B,p_NB,p_uB,Row,Col);

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&GPUtime, start, stop);

	MeanNormTime += GPUtime; 
	
	cudaThreadSynchronize();
	assert(cudaGetLastError() ==cudaSuccess);	
	
	cudaEventRecord(start, 0);

	threadsPerBlock.x = 64;
	threadsPerBlock.y = 1;
	blocksPerGrid.x = (unsigned int)Row;
	blocksPerGrid.y = (unsigned int)1;
	ZScore<<<blocksPerGrid, threadsPerBlock>>>(d_B,p_NB,p_uB,Row,Col);

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&GPUtime, start, stop);

	SummationTime += GPUtime; 

	cudaThreadSynchronize();
	assert(cudaGetLastError() ==cudaSuccess);	

	int shift;

	for (int i = 0;i<sp;i++)
	{
		//cout<<i<<endl;

		cudaEventRecord(start, 0);

		cublasSetMatrix (Col, Row/sp,sizeof(float),dataArray1+ i*(Data_size/sp), Col, d_A,Col);
		if (cublasGetError()!= CUBLAS_STATUS_SUCCESS)
		{
			std::cerr<<"cublasSetA has an Error!"<<std::endl;
			printf("\n%d\n",cublasGetError());		
			exit(EXIT_FAILURE);
		}	

		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);
		MemTransTime += GPUtime; 		

		

		cudaEventRecord(start, 0);

		threadsPerBlock.x = 64;
		threadsPerBlock.y = 1;
		blocksPerGrid.x = (unsigned int)Row/sp;
		blocksPerGrid.y = (unsigned int)1;
		MeanNormKernel<<<blocksPerGrid, threadsPerBlock>>>(d_A,p_NA,p_uA,Row/sp,Col);

		cudaThreadSynchronize();
		assert(cudaGetLastError() ==cudaSuccess);	

		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);
		MeanNormTime += GPUtime; 

		cudaEventCreate(&start);
		cudaEventCreate(&stop);
		cudaEventRecord(start, 0);

		threadsPerBlock.x = 64;
		threadsPerBlock.y = 1;
		blocksPerGrid.x = (unsigned int)Row/sp;
		blocksPerGrid.y = (unsigned int)1;
		ZScore<<<blocksPerGrid, threadsPerBlock>>>(d_A,p_NA,p_uA,Row,Col);

		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);
		
		SummationTime += GPUtime; 


		cudaEventRecord(start, 0);			

		cublasSgemm ('t', 'n', Row/sp,Row,Col, 1 , d_A, Col,d_B, Col, 0,p_Res, Row/sp);
		
		if (cublasGetError()!= CUBLAS_STATUS_SUCCESS)
		{
			std::cerr<<"cublasSgemm has an Error!"<<std::endl;
			printf("\n%d\n",cublasGetError());		
			exit(EXIT_FAILURE);
		}
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);
		
		GemmTime += GPUtime;



		cudaEventRecord(start, 0);
		threadsPerBlock.x = 64;
		threadsPerBlock.y = 1;
		blocksPerGrid.x = (unsigned int)Row/(sp*threadsPerBlock.x);
		blocksPerGrid.y = (unsigned int)Row;
		IndInitKernel<<<blocksPerGrid, threadsPerBlock>>>(p_RowInd,p_ColInd, RowF*Row+i*(Row/sp),ColF*Col,Row/sp);
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);
		SortInitTime += GPUtime;



		cudaEventRecord(start, 0);			

		thrust::sort_by_key(d_Res.begin(), d_Res.end(),make_zip_iterator(make_tuple(d_RowInd.begin(),d_ColInd.begin())));


		thrust::stable_sort_by_key(d_RowInd.begin(),d_RowInd.end(),make_zip_iterator(make_tuple(d_Res.begin(),d_ColInd.begin())));	


		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);
	
		SortingTime +=GPUtime;


		cudaEventRecord(start, 0);			

		shift = i*k*Row/sp;
		threadsPerBlock.x = (unsigned int)1;
		threadsPerBlock.y = (unsigned int)k;
		blocksPerGrid.x = (unsigned int)(floor(Row/(sp*threadsPerBlock.x))+1);
		blocksPerGrid.y = (unsigned int)1;
		CoalescedValMove<<<blocksPerGrid, threadsPerBlock>>>(p_Res,p_RowInd,p_ColInd,p_KNN1+shift, p_KNNRowInd1+shift,p_KNNColInd1+shift,Row/sp,k,Row);

		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);

		SortMoveTime+=GPUtime;	

		cudaEventRecord(start, 0);			
		thrust::sort_by_key(d_Res.begin(), d_Res.end(),make_zip_iterator(make_tuple(d_RowInd.begin(),d_ColInd.begin())));

		thrust::stable_sort_by_key(d_ColInd.begin(),d_ColInd.end(),make_zip_iterator(make_tuple(d_Res.begin(),d_RowInd.begin())));	

		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);
		SortingTime +=GPUtime;

		cudaEventRecord(start, 0);			
		shift = (i%2)*k*Row;
		threadsPerBlock.x = 1;
		threadsPerBlock.y = k;
		blocksPerGrid.x = (unsigned int)(floor(Row/threadsPerBlock.x)+1);
		blocksPerGrid.y = 1;
		CoalescedValMove<<<blocksPerGrid, threadsPerBlock>>>(p_Res,p_RowInd,p_ColInd,p_KNN2+shift, p_KNNRowInd2+shift,p_KNNColInd2+shift,Row,k,Row/sp);
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&GPUtime, start, stop);



		if ((i%2))
		{
			if (i ==1)
			{
				
				cudaEventRecord(start, 0);			
				thrust::sort_by_key(d_KNN2.begin(), d_KNN2.begin()+2*k*Row,make_zip_iterator(make_tuple(d_KNNRowInd2.begin(),d_KNNColInd2.begin())));

				thrust::stable_sort_by_key(d_KNNColInd2.begin(),d_KNNColInd2.begin()+2*k*Row,make_zip_iterator(make_tuple(d_KNN2.begin(),d_KNNRowInd2.begin())));
				cudaEventRecord(stop, 0);
				cudaEventSynchronize(stop);
				cudaEventElapsedTime(&GPUtime, start, stop);
				SortingTime +=GPUtime;

				cudaEventRecord(start, 0);			
				threadsPerBlock.x = 1;
				threadsPerBlock.y = k;
				blocksPerGrid.x = (unsigned int)(floor(Row/threadsPerBlock.x)+1);
				blocksPerGrid.y = 1;
				CoalescedValMove<<<blocksPerGrid, threadsPerBlock>>>(p_KNN2,p_KNNRowInd2,p_KNNColInd2,p_KNNt, p_KNNRowIndt,p_KNNColIndt,Row,k,k*2);	


				cudaEventRecord(stop, 0);
				cudaEventSynchronize(stop);
				cudaEventElapsedTime(&GPUtime, start, stop);
				SortMoveTime+=GPUtime;	

				cudaEventRecord(start, 0);			
				cudaMemcpy(p_KNN2+2*k*Row,p_KNNt,Row*k*sizeof(float),cudaMemcpyDeviceToDevice);
				cudaMemcpy(p_KNNRowInd2+2*k*Row,p_KNNRowIndt,Row*k*sizeof(int),cudaMemcpyDeviceToDevice);
				cudaMemcpy(p_KNNColInd2+2*k*Row,p_KNNColIndt,Row*k*sizeof(int),cudaMemcpyDeviceToDevice);
				cudaEventRecord(stop, 0);
				cudaEventSynchronize(stop);
				cudaEventElapsedTime(&GPUtime, start, stop);
				GPUMemTrans+=GPUtime;	
			}
			else
			{
				cudaEventRecord(start, 0);			
				thrust::sort_by_key(d_KNN2.begin(), d_KNN2.end(),make_zip_iterator(make_tuple(d_KNNRowInd2.begin(),d_KNNColInd2.begin())));

				thrust::stable_sort_by_key(d_KNNColInd2.begin(),d_KNNColInd2.end(),make_zip_iterator(make_tuple(d_KNN2.begin(),d_KNNRowInd2.begin())));
				cudaEventRecord(stop, 0);
				cudaEventSynchronize(stop);
				cudaEventElapsedTime(&GPUtime, start, stop);
				SortingTime +=GPUtime;


				cudaEventRecord(start, 0);			
				threadsPerBlock.x = 1;
				threadsPerBlock.y = k;
				blocksPerGrid.x = (unsigned int)(floor(Row/threadsPerBlock.x)+1);
				blocksPerGrid.y = 1;
				CoalescedValMove<<<blocksPerGrid, threadsPerBlock>>>(p_KNN2,p_KNNRowInd2,p_KNNColInd2,p_KNNt, p_KNNRowIndt,p_KNNColIndt,Row,k,k*3);	
				cudaEventRecord(stop, 0);
				cudaEventSynchronize(stop);
				cudaEventElapsedTime(&GPUtime, start, stop);
				SortMoveTime+=GPUtime;	

				cudaEventRecord(start, 0);			
				cudaMemcpy(p_KNN2+2*k*Row,p_KNNt,Row*k*sizeof(float),cudaMemcpyDeviceToDevice);
				cudaMemcpy(p_KNNRowInd2+2*k*Row,p_KNNRowIndt,Row*k*sizeof(int),cudaMemcpyDeviceToDevice);
				cudaMemcpy(p_KNNColInd2+2*k*Row,p_KNNColIndt,Row*k*sizeof(int),cudaMemcpyDeviceToDevice);
				cudaEventRecord(stop, 0);
				cudaEventSynchronize(stop);
				cudaEventElapsedTime(&GPUtime, start, stop);
				GPUMemTrans+=GPUtime;	
			}
		}


	}

	



	cudaEventRecord(start, 0);			

 	thrust::copy(d_KNN1.begin(), d_KNN1.end(), Key1);
 	thrust::copy(d_KNNRowInd1.begin(), d_KNNRowInd1.end(), Row1);
 	thrust::copy(d_KNNColInd1.begin(), d_KNNColInd1.end(), Col1);

 	thrust::copy(d_KNN2.begin()+2*k*Row, d_KNN2.end(), Key2);
 	thrust::copy(d_KNNRowInd2.begin()+2*k*Row, d_KNNRowInd2.end(), Row2);
 	thrust::copy(d_KNNColInd2.begin()+2*k*Row, d_KNNColInd2.end(), Col2);

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&GPUtime, start, stop);
	MemTransTime += GPUtime; 



	
	cublasFree(d_A);
	cublasFree(d_B);	
	
	cublasShutdown();



/*if (myid == 0)
	{
	cout<<RowF<<","<<ColF<<endl;
	cout<<"Initation took:"<<InitTime<<"ms"<<endl;
	cout<<"CPU/GPU Transfer took:"<<MemTransTime<<"ms"<<endl;
	cout<<"Gemm took:"<<GemmTime<<"ms"<<endl;
	cout<<"Norm took:"<<MeanNormTime<<"ms"<<endl;
	cout<<"Summation took:"<<SummationTime<<"ms"<<endl;
	cout<<"Sorting took:"<<SortingTime<<"ms"<<endl;
	cout<<"Index Initiation took:"<<SortInitTime<<"ms"<<endl;
	cout<<"Extracting Sorted Values took:"<<SortMoveTime<<"ms"<<endl;
	cout<<"GPU data transfer took"<<GPUMemTrans<<"ms"<<endl;
	}*/

}

void SortResults(int shift)
{
	cudaThreadSynchronize();
	assert(cudaGetLastError() ==cudaSuccess);

	thrust::device_vector<float> d_key(2*Th*height);
	thrust::device_vector<int> d_row(2*Th*height);
	thrust::device_vector<int> d_col(2*Th*height);

	float * Kptr = thrust::raw_pointer_cast(&d_key[0]);	
	int * VptrR = thrust::raw_pointer_cast(&d_row[0]);
	int * VptrC = thrust::raw_pointer_cast(&d_col[0]);

	cudaError_t res = cudaMemcpy(Kptr,ResultsKeys+shift,2*Th*height*sizeof(float),cudaMemcpyHostToDevice);
	assert(res==cudaSuccess);

	res = cudaMemcpy(VptrR,ResultsRows+shift,2*Th*height*sizeof(int),cudaMemcpyHostToDevice);
	assert(res==cudaSuccess);

	res = cudaMemcpy(VptrC,ResultsCols+shift,2*Th*height*sizeof(int),cudaMemcpyHostToDevice);
	assert(res==cudaSuccess);

	cudaThreadSynchronize();
	assert(cudaGetLastError() ==cudaSuccess);		

	thrust::sort_by_key(d_key.begin(), d_key.end(),make_zip_iterator(make_tuple(d_row.begin(),d_col.begin())));

	cudaThreadSynchronize();
	assert(cudaGetLastError() ==cudaSuccess);

	thrust::stable_sort_by_key(d_row.begin(),d_row.end(),make_zip_iterator(make_tuple(d_key.begin(),d_col.begin())));	

	cudaThreadSynchronize();
	assert(cudaGetLastError() ==cudaSuccess);

	thrust::device_vector<float> d_CFkey(Th*height);
	thrust::device_vector<int> d_CFvalR(Th*height);
	thrust::device_vector<int> d_CFvalC(Th*height);

	float * CFKptr = thrust::raw_pointer_cast(&d_CFkey[0]);	
	int * CFVptrR = thrust::raw_pointer_cast(&d_CFvalR[0]);
	int * CFVptrC = thrust::raw_pointer_cast(&d_CFvalC[0]);
	dim3 threadsPerBlock,blocksPerGrid;
	threadsPerBlock.x = (unsigned int)1;
	threadsPerBlock.y = (unsigned int)Th;
	blocksPerGrid.x = (unsigned int)(floor(height/(threadsPerBlock.x))+1);
	blocksPerGrid.y = (unsigned int)1;

	cudaThreadSynchronize();
	assert(cudaGetLastError() ==cudaSuccess);
	CoalescedValMoveF<<<blocksPerGrid, threadsPerBlock>>>(Kptr,VptrR,VptrC,CFKptr,CFVptrR,CFVptrC,height,0,Th,2*Th);

	cudaThreadSynchronize();
	assert(cudaGetLastError() ==cudaSuccess);

	thrust::copy(d_CFkey.begin(), d_CFkey.end(),ResultsKeys+shift);

	thrust::copy(d_CFvalR.begin(), d_CFvalR.end(),ResultsRows+shift);

	thrust::copy(d_CFvalC.begin(), d_CFvalC.end(), ResultsCols+shift);

	cudaThreadSynchronize();
	assert(cudaGetLastError() ==cudaSuccess);


}


void allocatePinnedMem()
{	


	cudaSetDeviceFlags(cudaDeviceMapHost);
	cudaError_t err=cudaGetLastError();
	assert(err==cudaSuccess);
//1.478 GB
	cudaError_t res = cudaHostAlloc((void**)&(dataArray1), 
		DATA_SIZE*sizeof(float), 
		cudaHostAllocDefault|cudaHostAllocMapped|cudaHostAllocPortable);

	if(res!=cudaSuccess)
	{
		std::cerr<<"dataArray1 Can not allocate enough pinned memory"<<std::endl;
		exit(EXIT_FAILURE);
	}
//1.478 GB
	res = cudaHostAlloc((void**)&(dataArray2), 
		DATA_SIZE*sizeof(float), 
		cudaHostAllocDefault|cudaHostAllocMapped|cudaHostAllocPortable);

	if(res!=cudaSuccess)
	{
		std::cerr<<"data Array2 Can not allocate enough pinned memory"<<std::endl;
		exit(EXIT_FAILURE);
	}
	
//2.328 GB
	res = cudaHostAlloc((void**)&(resultsArray), 
		RESULTS_SIZE*sizeof(float), 
		cudaHostAllocDefault|cudaHostAllocMapped|cudaHostAllocPortable);

	if(res!=cudaSuccess)
	{
		std::cerr<<"resultsArray Can not allocate enough pinned memory"<<std::endl;
		exit(EXIT_FAILURE);
	}

//2.328 GB
	res = cudaHostAlloc((void**)&(ResultsKeys),2*(numfiles/numprocs)*height*Th*sizeof(float),cudaHostAllocDefault|cudaHostAllocMapped|cudaHostAllocPortable);

	if(res!=cudaSuccess)
	{
		std::cerr<<"ResultsKeys Can not allocate enough pinned memory"<<std::endl;
		exit(EXIT_FAILURE);
	}

//2.328 GB
	res = cudaHostAlloc((void**)&(ResultsRows),2*(numfiles/numprocs)*height*Th*sizeof(int),cudaHostAllocDefault|cudaHostAllocMapped|cudaHostAllocPortable);

	if(res!=cudaSuccess)
	{
		std::cerr<<"ResultsRows Can not allocate enough pinned memory"<<std::endl;
		exit(EXIT_FAILURE);
	}

//2.328 GB
	res = cudaHostAlloc((void**)&(ResultsCols), 2*(numfiles/numprocs)*height*Th*sizeof(int),cudaHostAllocDefault|cudaHostAllocMapped|cudaHostAllocPortable);

	if(res!=cudaSuccess)
	{
		std::cerr<<"ResultsCols Can not allocate enough pinned memory"<<std::endl;
		exit(EXIT_FAILURE);
	}
//23.841 MB
	res = cudaHostAlloc((void**)&Key1, 
		Th*height*sizeof(float), 
		cudaHostAllocDefault|cudaHostAllocMapped|cudaHostAllocPortable);

	if(res!=cudaSuccess)
	{
		std::cerr<<"Key1 Can not allocate enough pinned memory"<<std::endl;
		exit(EXIT_FAILURE);
	}

//23.841 MB	
	res = cudaHostAlloc((void**)&Row1, 
		Th*height*sizeof(int), 
		cudaHostAllocDefault|cudaHostAllocMapped|cudaHostAllocPortable);

	if(res!=cudaSuccess)
	{
		std::cerr<<"Row1 Can not allocate enough pinned memory"<<std::endl;
		exit(EXIT_FAILURE);
	}

//23.841 MB
	res = cudaHostAlloc((void**)&Col1, 
		Th*height*sizeof(int), 
		cudaHostAllocDefault|cudaHostAllocMapped|cudaHostAllocPortable);

	if(res!=cudaSuccess)
	{
		std::cerr<<"Col1 Can not allocate enough pinned memory"<<std::endl;
		exit(EXIT_FAILURE);
	}

//23.841 MB
	res = cudaHostAlloc((void**)&Key2, 
	Th*height*sizeof(float), 
	cudaHostAllocDefault|cudaHostAllocMapped|cudaHostAllocPortable);
	if(res!=cudaSuccess)
	{
		std::cerr<<"Key2 Can not allocate enough pinned memory"<<std::endl;
		exit(EXIT_FAILURE);
	}

//23.841 MB	
	res = cudaHostAlloc((void**)&Row2, 
		Th*height*sizeof(int), 
		cudaHostAllocDefault|cudaHostAllocMapped|cudaHostAllocPortable);

	if(res!=cudaSuccess)
	{
		std::cerr<<"Row2 Can not allocate enough pinned memory"<<std::endl;
		exit(EXIT_FAILURE);
	}

//23.841 MB
	res = cudaHostAlloc((void**)&Col2, 
		Th*height*sizeof(int), 
		cudaHostAllocDefault|cudaHostAllocMapped|cudaHostAllocPortable);

	if(res!=cudaSuccess)
	{
		std::cerr<<"Col2 Can not allocate enough pinned memory"<<std::endl;
		exit(EXIT_FAILURE);
	}
}
void freeMem()
{
	cudaFreeHost(dataArray1);
	cudaFreeHost(dataArray2);	
	cudaFreeHost(resultsArray);
	cudaFreeHost(Key1);
	cudaFreeHost(Key2);
	cudaFreeHost(Row1);
	cudaFreeHost(Row2);
	cudaFreeHost(Col1);
	cudaFreeHost(Col2);
}
void freeRes()
{
	cudaFreeHost(ResultsKeys);
	cudaFreeHost(ResultsRows);
	cudaFreeHost(ResultsCols);

}

